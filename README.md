# Scrapebys

Scrapebys is a custom web scraper written in Python 3 to collect images (as well as some metadata) from the Sotheby's art auction house website. 

## Installation

Scapebys is still in sporadic development, and is best run in a virtual environment for the moment:

```
git clone https://bitbucket.org/ds0/scrapebys.git
cd scrapebys
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
```

## Usage

You can take an art auction's URL to download all of its images. All of the artwork is saved to a new directory with a ```lots.json``` file containing metadata about each work. You can add the ```-a``` flag to only download artwork, or the ```-j``` flag to only download the metadata file.

```
scrapebys.py http://www.sothebys.com/en/auctions/2009/important-old-master-paintings-including-european-works-of-art-n08516.html
```

You can also use Scrapebys to generate a JSON file containing the auctions you wish to download images from. This JSON file is named ```auctions.json``` by default.

```
scrapebys.py get-auctions
scrapebys.py auctions.json
```

You can ```--filter``` or ```--ignore``` comma-separated substrings that are found in the title of auctions when generating a JSON file.

```
scrapebys.py get-auctions -f "watch, clock, time" -n watches.json
scrapebys.py get-auctions -i "wine, cellar" -n no_wine.json
```

## Command list

```
Scrapebys - Sotheby's auction artwork scraper.
You can use either a Sotheby's auction URL or a pre-generated JSON file to
download its images. You can also use the get-auctions option to generate a JSON
file containing the auctions you wish to download.

Usage:
    scrapebys.py get-auctions [-hv] [-d DIR] [-n NAME] [-i IGNORE] [-f FILTER]
    scrapebys.py AUCTION_SOURCE [-hv] [-j | -a] [-d DIR]

-h --help               display usage menu and quit
-v --verbose            verbose output
-d DIR --directory DIR  save results to a specific directory [default: ./]
-j --json-only          only save a JSON file of an auction (no images)
-a --art-only           only save artwork of an auction (no JSON file)

get-auctions options:
-n NAME --name NAME         save auctions list with a specific name [default: auctions.json]
                            (can be used as AUCTION_SOURCE)
-i IGNORE --ignore IGNORE   do not save auctions that contain specific comma-separated keywords
-f FILTER --filter FILTER   only download auctions that contain specific comma-separated keywords
                            (overwritten by ignored keywords)
```

