"""Scrapebys - Sotheby's auction artwork scraper.
You can use either a Sotheby's auction URL or a pre-generated JSON file to
download its images. You can also use the get-auctions option to generate a JSON
file containing the auctions you wish to download.

Usage:
    scrapebys.py get-auctions [-hv] [-d DIR] [-n NAME] [-i IGNORE] [-f FILTER]
    scrapebys.py AUCTION_SOURCE [-hv] [-j | -a] [-d DIR]

-h --help               display usage menu and quit
-v --verbose            verbose output
-d DIR --directory DIR  save results to a specific directory [default: ./]
-j --json-only          only save a JSON file of an auction (no images)
-a --art-only           only save artwork of an auction (no JSON file)

get-auctions options:
-n NAME --name NAME         save auctions list with a specific name [default: auctions.json]
                            (can be used as AUCTION_SOURCE)
-i IGNORE --ignore IGNORE   do not save auctions that contain specific comma-separated keywords
-f FILTER --filter FILTER   only download auctions that contain specific comma-separated keywords
                            (overwritten by ignored keywords)
"""
from docopt import docopt
import time
import argparse
import requests
import json
from slugify import slugify
import os

verbose = True

# scrape basic auction details from present day towards beginning of time
# TODO: only get unloaded auctions
def get_auctions_list(ignore_list,filter_list):
    result = []
    end_date = int(time.time())*1000
    while end_date != 0:
        url = "http://www.sothebys.com/api/ajax.auctions.json/lang=en&startDate=0&endDate={}&orderBy=date&ascing=desc&showPast=true".format(end_date)
        auction_json = requests.get(url, headers=headers).text
        auction_json = json.loads(auction_json)
        # Sotheby's limits the number of results to 500 per request:
        # Once we get less than that we can finish up.
        if len(auction_json['events']) < 500:
            end_date = 0
        else:
            end_date = auction_json['events'][-1]['startTimeStampInMilliSecs']
        # Add auction to list
        for event in auction_json['events']:
            if verbose:
                print(event["title"])
            title = event["title"]
            slug = event["eventResourcePath"][event["eventResourcePath"].rfind("/")+1:]
            auction_url = event["linkToDetailPage"]
            auction = {
                "title" : title,
                "slug" : slug,
                "url" : auction_url
            }
            passes_filter = False
            for filter_keyword in filter_list:
                if filter_keyword.lower() in title.lower():
                    passes_filter= True
                    break
            do_append = passes_filter
            if do_append:
                for ignore_keyword in ignore_list:
                    if ignore_keyword.lower() in title.lower():
                        do_append = False
                        break
            if do_append:
                result.append(auction)
    return result

def get_auction_from_url(url):
    auction = {}
    if "www.sothebys.com" not in url:
        print("Error: not a valid Sotheby's URL.")
    else:
        page = requests.get(url, headers=headers).text
        title = ""
        for line in page.split("\n"):
            if "<title>" in line:
                title = line.split("|")[0][7:].rstrip()
        slug = url[url.rfind("/"):-5]
        auction = {
            'title': title,
            'slug': slug,
            'url': url[23:]
        }
    return auction 

def get_auction_json(auction):
    lots_dict = {}
    if auction['url'] and 'lot.1.html' not in auction['url']:
        full_url = "http://www.sothebys.com{}".format(auction['url'])
        lots = []
        page = requests.get(full_url, headers=headers).text
        for line in page.split('\n'):
            # grab the line in the HTML file that contains all the lots
            if line.lstrip().startswith("ECAT.lot['1']"):
                raw_lots = line.lstrip()
                # split into individual lots
                for raw_lot in raw_lots.split("};")[:-1]:
                    # remove the "ECAT.lot['x']" portion of text
                    raw_lot = raw_lot[raw_lot.index("=")+1:]+"}"
                    # split lot into its attributes (author, title, etc)
                    raw_lot = raw_lot.replace(",'", "\n'").split("\n")
                    parsed_lot = {}
                    for attr in raw_lot:
                        split_attr = attr.split("'")
                        if len(split_attr) == 5:
                            parsed_lot[split_attr[1]] = split_attr[3]
                        elif len(split_attr) == 3:
                            parsed_lot[split_attr[1]] = split_attr[2][1:].rstrip()
                    for key, value in parsed_lot.items():
                        if value == "[]":
                            parsed_lot[key] = ""
                    lots.append(parsed_lot)
                break
        # now just write it to a file
        if verbose:
            print(auction['title'])
        lots_dict = {
            "title": auction["title"],
            "slug": auction["slug"],
            "url": auction["url"],
            "lots": lots
        }
    return lots_dict

def get_lots(lots_dict, target_dir):
    for lot in lots_dict['lots']:
        # don't load blank images, don't load copyright placeholders, don't drink and drive
        if "underCopyright" in lot["image"] or "lot.jpg" in lot["image"] or lot["isWine"] == "true":
            continue
        img_url = "http://www.sothebys.com"+lot["image"]
        lot_id = lot["lotItemId"]
        author = "unknown-author"
        if "guaranteeLine" in lot:
            author = lot["guaranteeLine"] if lot["guaranteeLine"] != "" else "unknown-author"
        title = "untitled-work"
        if "title" in lot:
            title = lot["title"] if lot["title"] != "" else "untitled-work"
        filename = "{}_{}_{}.jpg".format(slugify(author), slugify(title), lot_id)
        r = requests.get(img_url, headers=headers, stream=True)
        r.raise_for_status()
        # don't bother loading images that already exist
        if filename not in os.listdir(target_dir):
            with open(target_dir+"/"+filename, "wb") as handle:
                for block in r.iter_content(1024):
                    handle.write(block)
            if verbose:
                print(filename)

# TODO: find way for header to work over obvious servers
# spoof header to prevent blocking of requests
headers = {'Host':'www.sothebys.com', 'User-Agent':'Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0'}

if __name__ == "__main__":
    args = docopt(__doc__)
    verbose = args['--verbose']
    print(args)
    if args['--directory'][-1] != "/":
        args['--directory'] += "/"
    if args['get-auctions']:
        filename = args['--directory'] + args['--name']
        ignore_list = [] if not args['--ignore'] else args['--ignore'].split(',')
        filter_list = [] if not args['--filter'] else args['--filter'].split(',')
        print(filter_list)
        auctions_list = get_auctions_list(ignore_list, filter_list)
        with open(filename, 'w') as outfile:
            json.dump(auctions_list, outfile, indent=4)
    else:
        auctions = []
        json_source = args['AUCTION_SOURCE'][-5:] == ".json"
        if json_source:
            with open(args['AUCTION_SOURCE'], 'r') as infile:
                auctions = json.load(infile)
        else:
            auctions = [get_auction_from_url(args['AUCTION_SOURCE'])]
        for auction in auctions:
            if auction:
                lots_dict = get_auction_json(auction)
                if lots_dict:
                    target_dir = args['--directory']+lots_dict['slug']
                    if not args['--json-only']:
                        if not os.path.exists(target_dir):
                            os.makedirs(target_dir)
                        get_lots(lots_dict, target_dir)
                        if args['--art-only']:
                            if not os.listdir(target_dir):
                                os.rmdir(target_dir)
                    if not args['--art-only']:
                        path = args['--directory']+lots_dict['slug']+'/lots.json'
                        if args['--json-only']:
                            path = args['--directory']+lots_dict['slug']+'.json'
                        with open(path, 'w') as outfile:
                            json.dump(lots_dict, outfile, indent=4)
